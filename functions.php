<?php declare (strict_types = 1);

const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DATABASE = 'baltic_talents';

const NPD = 149;
const INCOME_TAX_PERCENT = 0.15;
const HEALTH_INSURANCE_PERCENT = 0.06;
const SOCIAL_INSURANCE_PERCENT = 0.06;
const SODRA_PERCENT = 0.3098;
const WARANTY_FUND_PERCENT = 0.002;


function getConnection(): PDO 
{
    return new PDO('mysql:host=' . HOST .';dbname=' . DATABASE .';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

function getEmployeesList(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT id, name, surname, birthday, salary FROM darbuotojai');
    $query->execute();
    return $query->fetchAll();
}

function getPositionsList(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $query->execute();
    return $query->fetchAll();
}

function getEmployee(PDO $pdo, int $id): array
{
    $query = $pdo->prepare('SELECT * FROM darbuotojai WHERE id =:id');
    $query->execute(['id' => $id]);
    return $query->fetchAll();
   
}

function getEmployeesByPosition($pdo, $position_id): array
{
    $query = $pdo->prepare('SELECT id, name, surname, birthday, education, salary, phone FROM darbuotojai WHERE pareigos_id =:id');
    $query->execute(['id' => $position_id]);
    return $query->fetchAll();
}

function getPositionName($pdo, $position_id) 
{    
    $query = $pdo->prepare('SELECT id, name FROM pareigos WHERE id =:id');
    $query->execute(['id' => $position_id]);
    return $query->fetch();
}

function getPositionsCount($pdo):array
{
    $query = $pdo->prepare('SELECT pareigos_id, COUNT(*) FROM darbuotojai GROUP BY pareigos_id');
    $query->execute();
    return $query->fetchAll(PDO::FETCH_KEY_PAIR); 
}

function getTaxesForEmployee($salary): array
{
    $incomeTax = ($salary - NPD) * INCOME_TAX_PERCENT;
    $healthSecurityTax = $salary * HEALTH_INSURANCE_PERCENT;
    $socialSecurityTax = $salary * SOCIAL_INSURANCE_PERCENT;
    $salaryAfterTaxes = $salary - $incomeTax - $healthSecurityTax - $socialSecurityTax;

    $SODRA = $salary * SODRA_PERCENT;
    $fund = $salary * WARANTY_FUND_PERCENT;
    $totalTaxesForEmployee = $salary + $SODRA + $fund;

    return ['income_tax' => $incomeTax,
    'health_security_tax' => $healthSecurityTax,
    'social_security_tax' => $socialSecurityTax,
    'salary_after_taxes' => $salaryAfterTaxes,
    'SODRA' => $SODRA,
    'fund' => $fund,
    'total' => $totalTaxesForEmployee,
    ];
}

function getStatisticsByEducation(PDO $pdo): array
{
    $query = $pdo->prepare('SELECT education, COUNT(*) employee_count, AVG(salary) average_salary FROM darbuotojai GROUP BY education');
    $query->execute();
    return $query->fetchAll();
}


function getTotalEmployeeCount(PDO $pdo): int
{
    $query = $pdo->prepare('SELECT COUNT(*) employee_count FROM darbuotojai');
    $query->execute();
    $result = $query->fetch();

    return (int) $result['employee_count'];
}

function getStatisticsByGender(PDO $pdo)
{
    $query = $pdo->prepare('SELECT gender, COUNT(*) employee_count FROM darbuotojai GROUP BY gender');
    $query->execute();

    return $query->fetchAll();
}


