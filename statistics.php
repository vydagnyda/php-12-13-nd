<?php require_once 'functions.php';
$pdo = getConnection();
$statisticsByEducation = getStatisticsByEducation($pdo);
$employeeCount = getTotalEmployeeCount($pdo);
$statisticsByGender = getStatisticsByGender($pdo);
?>

<html>
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <a href="index.php">Atgal</a>
        <h1 text-align="center">Įmonės statistika</h1>
        <br>
        <h4>Pagal išsilavinimą</h4>
        <table class="table">     
            <tr>             
                <th scope="col">Išsilavinimas</th>
                <th scope="col">Darbuotojų skaičius</th>
                <th scope="col">Vidutinis darbo užmokestis</th>
            <?php foreach ($statisticsByEducation as $educationStatisticsItem) {?>
            <tr>
                <td><?php echo $educationStatisticsItem['education']; ?></td>
                <td><?php echo $educationStatisticsItem['employee_count']; ?></td>
                <td><?php echo round($educationStatisticsItem['average_salary'], 2); ?></td>
            </tr>
            <?php }?>
            </tr> 
        </table>  
        <br>

        <h4>Pagal lytį</h4>
        <table class="table">     
            <tr>
                <th scope="col">Lytis</th>
                <th scope="col">Darbuotojų skaičius</th>
                <th scope="col">Kiekis procentais</th>
                <?php foreach ($statisticsByGender as $genderStatisticsItem) {?>
            <tr>
                <td><?php echo $genderStatisticsItem['gender']; ?></td>
                <td><?php echo $genderStatisticsItem['employee_count']; ?></td>
                <td><?php echo round(100 * $genderStatisticsItem['employee_count'] / $employeeCount, 2); ?>%</td>
            </tr>
            <?php }?>
            </tr> 
        </table>  
    </div>
</body>
</html>
